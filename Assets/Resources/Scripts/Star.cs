﻿using System.Collections;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using Random = UnityEngine.Random;

public class Star : MonoBehaviour
{
    private GameObject _lightStar;
    private float _intence;
    private char _type;
    private string _starName;
    private int _temp;
    private Color _starColor;
    private bool _havePlanet;
    private GameObject _starParticle;
    private float _mass;
    private Vector3 _starRotation;
    private GameObject _blackHoll;
    private Vector3 _blackHollPos;   
    
    // Start is called before the first frame update
    void Start()
    {
        name = _starName;
        var transformPuls = transform;
        _starRotation = new Vector3(0.0f, 0.005f, 0.0f);
        var localScale = transformPuls.localScale;            
        
        _blackHoll = GameObject.Find("WorldGenerator");
        _blackHollPos = _blackHoll.transform.position;
        
        if (gameObject.GetComponent<Renderer>() != null)
        {
            gameObject.GetComponent<Renderer>().material.enableInstancing = true;
            gameObject.GetComponent<Renderer>().material.color = _starColor;
        }
        else
        {
            _starParticle = Resources.Load("Prefabs/Star") as GameObject;
 //           _lightStar = Resources.Load("Prefabs/starLight") as GameObject;
 //          _lightStar.GetComponent<Light>().intensity = _intence;
            var pulsing = Instantiate(_starParticle, transformPuls, true);
            pulsing.transform.position = transformPuls.position;
            pulsing.transform.localScale = transformPuls.localScale;
            pulsing.transform.GetChild(0).GetComponent<ParticleSystem>().transform.localScale = localScale;
//            pulsing.transform.GetChild(1).GetComponent<ParticleSystem>().transform.localScale = localScale;
//            var color0 = pulsing.transform.GetChild(0).GetComponent<ParticleSystem>().textureSheetAnimation;
//            color0.startFrame = 0;
            pulsing.transform.GetChild(0).GetComponent<ParticleSystem>().startColor = _starColor;
//            pulsing.transform.GetChild(1).GetComponent<ParticleSystem>().startColor = _starColor;
        }

        if (_havePlanet)
        {
            var havePlanetChance = Random.Range(0f, 1f);
            if (havePlanetChance >= 0.75f)
            {
                    var magicCalc = (int) (_intence * 8);
                    while (magicCalc != 0)
                    {
                        var planet = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        planet.AddComponent<Planet>();
                        if (Random.Range(0f, 1f) >= 0.5)
                        {
                            planet.GetComponent<Planet>().SetType('E');
                        }
                        else
                        {
                            planet.GetComponent<Planet>().SetType('G');
                        }
                        var planetComp = planet.GetComponent<Planet>();
                        planetComp.SetPlanetName(planetNameGenerator(planetComp.GetPType()));
                        Vector3 position;
                        position = Random.insideUnitCircle * 1000;
                        position = new Vector3(position.x, 0f, position.y);
                        position += new Vector3(transformPuls.position.x, transformPuls.position.y, transformPuls.position.z);
//                        position = new Vector3(position.x, position.z, position.y);
                        planet.transform.position = position;
                        if (planetComp.GetPType() == 'E')
                        {
                            var magicRandom = Random.Range(0.5f, 10.5f);
                            planet.transform.localScale = new Vector3(magicRandom, magicRandom, magicRandom);
                            planetComp.SetMass(Random.Range(1f, 18f));
                            if (Mathf.Abs(transform.position.z - planet.transform.position.z) > 35f &&
                                Mathf.Abs(transform.position.z - planet.transform.position.z) < 350f)
                            {
                                if (planetComp.GetMass() > 3f)
                                {
                                    planetComp.SetEarth(true);
                                    _blackHoll.GetComponent<StarsGeneration>().AddExo();
                                }
                            }
                        }
                        else
                        {
                            var magicRandom = Random.Range(5f, 22.5f);
                            planet.transform.localScale = new Vector3(magicRandom, magicRandom, magicRandom);
                            planetComp.SetMass(Random.Range(15f, 150f));
                        }                       
                        planet.transform.localEulerAngles = new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f),
                            Random.Range(0f, 360f));
                        planetComp.SetParStar(gameObject);
                        _blackHoll.GetComponent<StarsGeneration>().AddPlanet();
                        magicCalc--;
                    }
            }
        }

    }

    private string planetNameGenerator(char typeX)
    {
        var starN = "" + typeX;
        var charCount = Random.Range(1, 16);
        while (charCount != 0)
        {
            starN += Random.Range(0, 9);
            charCount--;
        }
        return starN;
    }
    
    public void SetMass(float x)
    {
        _mass = x;
    }

    public void SetType(char x)
    {
        _type = x;
    }

    public void SetStarName(string x)
    {
        _starName = x;
    }

    public void SetTemp(int x)
    {
        _temp = x;
    }

    public void SetPlanet(bool x)
    {
        _havePlanet = x;
    }

    public void SetStarColor(Color x)
    {
        _starColor = x;
    }

    public char GetPType()
    {
        return _type;
    }

    public int GetTemp()
    {
        return _temp;
    }

    public void SetIntence(float x)
    {
        _intence = x;
    }

    private void OnBecameVisible()
    {
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                MainThreadDispatcher.StartUpdateMicroCoroutine(AroundR());
                MainThreadDispatcher.StartUpdateMicroCoroutine(Rot());
            });
    }

    private IEnumerator AroundR()
    {
        transform.RotateAround(Vector3.zero, Vector3.up, 20 * (Time.deltaTime / _mass));
        yield return null;
    }

    private IEnumerator Rot()
    {
        transform.Rotate(_starRotation);
        yield return null;
    }
    
}
