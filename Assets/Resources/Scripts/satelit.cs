﻿using System.Collections;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class satelit : MonoBehaviour
{
    private float _mass;
    private Vector3 _starRotation;
    private Vector3 _rotator;
    private string _parPlanet;
    private GameObject _parentPlanet;
    // Start is called before the first frame update
    void Start()
    {
        _starRotation = new Vector3(0.0f, Random.Range(0f, 1f), 0.0f);
        gameObject.GetComponent<Renderer>().material.enableInstancing = true;
        var position = transform.position;
        _rotator = new Vector3(0f, 1 - position.y, position.y);
        if (transform.position.z < 0)
        {
            _rotator.z *= -1;
        } 
        
    }

    public void SetMass(float x)
    {
        _mass = x;
    }
    
    private void OnBecameVisible()
    {
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                MainThreadDispatcher.StartUpdateMicroCoroutine(aroundR());
                MainThreadDispatcher.StartUpdateMicroCoroutine(Rot());
        });
    }
    
    IEnumerator aroundR()
    {
        transform.RotateAround(_parentPlanet.transform.position, _rotator, 20 * Time.deltaTime / _mass);
        yield return null;
    }

    IEnumerator Rot()
    {
        transform.Rotate(_starRotation);
        yield return null;
    }
    public void SetParPlanet(GameObject x)
    {
        _parentPlanet = x;
    }
}
